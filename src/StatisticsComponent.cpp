#include "StatisticsComponent.hpp"

#include <QChartView>
#include <QLineSeries>
#include <QBarCategoryAxis>
#include <QValueAxis>
#include <QDateTimeAxis>

#include <QPair>
#include <QDateTime>

#include <QPainter>

#include <QVBoxLayout>
#include <QFormLayout>

#include <QSqlQuery>

#include <QDebug>

// TODO: Make it be only one type of romanian currency name(not LEI and RON) and to be set at the initial setup screen

StatisticsComponent::StatisticsComponent(Settings& settings, QWidget *parent)
    : QWidget(parent)
    , m_settings { settings }
{
    m_chart->setAnimationOptions(QtCharts::QChart::SeriesAnimations);
    m_chart->legend()->setVisible(true);
    m_chart->legend()->setAlignment(Qt::AlignBottom);

    QPointer<QtCharts::QChartView> chartView = new QtCharts::QChartView(m_chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    QPointer<QFormLayout> currencyTypeLayout = new QFormLayout;
    currencyTypeLayout->addRow("Moneda:", m_currencyTypeCB);

    QPointer<QVBoxLayout> mainLayout = new QVBoxLayout;
    mainLayout->addLayout(currencyTypeLayout);
    mainLayout->addWidget(chartView);

    this->setLayout(mainLayout);

    connect(m_currencyTypeCB, &QComboBox::currentTextChanged, [=](const QString& currency){
        loadStatisticsForCurrency(currency);
    });
}

void StatisticsComponent::setDatabase(const std::shared_ptr<QSqlDatabase> db) {
    m_db = db;
}

void StatisticsComponent::setDate(int month, int year) {   
    m_isMonthlyStatistics = true;
    m_values.clear();

    m_month = month;
    m_year  = year;

    auto yearString = QString::number(year);
    auto monthString = ((month < 10) ? "0" : "") + QString::number(month);

    auto tableName = "t" + yearString;

    loadData(
        "SELECT `Suma`,`Data` FROM `" + tableName + "` WHERE `Data` LIKE \"%/" + monthString + "/" + yearString + "\" ORDER BY `Data` ASC",
        "SELECT `Suma`,`Data` FROM `BaniColecta` WHERE `Data` LIKE \"%/" + monthString + "/" + yearString + "\" ORDER BY `Data` ASC"
    );
}

void StatisticsComponent::setDate(int year) {
    m_isMonthlyStatistics = false;
    m_values.clear();

    m_year  = year;

    auto yearString = QString::number(year);
    auto tableName = "t" + yearString;

    loadData(
        "SELECT `Suma`,`Data` FROM `" + tableName + "` ORDER BY `Data` ASC",
        "SELECT `Suma`,`Data` FROM `BaniColecta` WHERE `Data` LIKE \"%/" + yearString + "\" ORDER BY `Data` ASC"
    );
}

void StatisticsComponent::loadData(const QString& plicuriSql, const QString& colectaSql) {
    QStringList currencies;

    auto insertMoneyToMap = [this](const QString &date, QHash<QString, int> money, int type){
        if (m_values.find(date) != m_values.end()) {
            if (type == 0) m_values[date].first = money;
            else           m_values[date].second = money;
        } else {
            if (type == 0) m_values.insert(date, { money, QHash<QString, int>() });
            else           m_values.insert(date, { QHash<QString, int>(), money });
        }
    };

    auto getValuesFromQuery = [&insertMoneyToMap, &currencies, this](QSqlQuery& query, int type){
        QString lastDate;

        QHash<QString, int> currentMoney;

        auto addToCurrentMoney = [&currentMoney, &currencies, this](const QString& moneyString){
            auto moneyMap = convertDatabaseMoney(moneyString);

            for (const auto& currency : moneyMap.keys()) {
                if (!currencies.contains(currency)) currencies.append(currency);
            }

            for (const auto& moneyKey : moneyMap.keys()) {
                if (currentMoney.find(moneyKey) == currentMoney.end()) {
                    currentMoney.insert(moneyKey, moneyMap[moneyKey]);
                } else {
                    currentMoney[moneyKey] += moneyMap[moneyKey];
                }
            }
        };

        while (query.next()) {
            auto currentDate = query.value(1).toString();

            if (currentDate == lastDate) {
                auto money = convertDatabaseMoney(query.value(0).toString());

                addToCurrentMoney(query.value(0).toString());
            } else {
                if (lastDate.length()) insertMoneyToMap(lastDate, currentMoney, type);

                currentMoney.clear();
                addToCurrentMoney(query.value(0).toString());

                lastDate = currentDate;
            }
        }

        insertMoneyToMap(lastDate, currentMoney, type);
    };

    QSqlQuery plicuriQuery(plicuriSql, *m_db);
    getValuesFromQuery(plicuriQuery, 0);

    QSqlQuery colectaQuery(colectaSql, *m_db);
    getValuesFromQuery(colectaQuery, 1);

    m_currencyTypeCB->clear();
    m_currencyTypeCB->addItems(currencies);

    loadStatisticsForCurrency(currencies[0]);
}

QHash<QString, int> StatisticsComponent::convertDatabaseMoney(const QString &s) {
    QHash<QString, int> money;

    auto sums = s.split(",");
    for (const auto& sum : sums) {
        auto parts = sum.split(" ");

        money.insert(parts[1], parts[0].toInt());
    }

    return money;
}

void StatisticsComponent::loadStatisticsForCurrency(const QString& currency) {
    int highestValue = 0;

    QPointer<QtCharts::QLineSeries> plicuriSeries = new QtCharts::QLineSeries();
    QPointer<QtCharts::QLineSeries> colectaSeries = new QtCharts::QLineSeries();

    plicuriSeries->setName("Plicuri");
    colectaSeries->setName("Colecta");

    auto dates = m_values.keys();

    QStringList axisXTexts;
    if (m_isMonthlyStatistics) {
        qSort(dates.begin(), dates.end(), [](const QString& d1, const QString& d2)->bool{
            return d1.mid(0,2).toInt() < d2.mid(0,2).toInt();
        });

        int x = 0;
        for (const auto& date : dates) {
            plicuriSeries->append(x, m_values[date].first[currency]);
            colectaSeries->append(x, m_values[date].second[currency]);

            if (highestValue < m_values[date].first[currency]) highestValue = m_values[date].first[currency];
            if (highestValue < m_values[date].second[currency]) highestValue = m_values[date].second[currency];

            ++x;
        }

        for (const auto& date : dates) {
            axisXTexts.append(date.mid(0, 5));
        }
    } else {
        axisXTexts << "Ianuarie" << "Februarie" << "Martie" << "Aprilie" << "Mai" << "Iunie" << "Iulie" << "August" << "Septembrie" << "Octombrie" << "Noiembrie" << "Decembrie";

        QHash<int, QPair<int, int>> v;
        for (int i=0; i < 12; ++i) v[i] = {0,0};

        for (const auto& date : dates) {
            int monthIndex = date.mid(3,2).toInt();
            v[monthIndex].first += m_values[date].first[currency];
            v[monthIndex].second += m_values[date].second[currency];
        }

        for (int i=0; i < 12; ++i) {
            plicuriSeries->append(i, v[i].first);
            colectaSeries->append(i, v[i].second);

            if (highestValue < v[i].first) highestValue = v[i].first;
            if (highestValue < v[i].second) highestValue = v[i].second;
        }
    }

    clearChart();
    m_chart->addSeries(plicuriSeries);
    m_chart->addSeries(colectaSeries);
//    m_chart->setTitle("Statistici pentru " + monthString + "/" + yearString);

    QPointer<QtCharts::QBarCategoryAxis> axisX = new QtCharts::QBarCategoryAxis();
    axisX->append(axisXTexts);
    m_chart->addAxis(axisX, Qt::AlignBottom);
    plicuriSeries->attachAxis(axisX);
    colectaSeries->attachAxis(axisX);
    axisX->setRange(axisXTexts.first(), axisXTexts.last());

    QPointer<QtCharts::QValueAxis> axisY = new QtCharts::QValueAxis;
    axisY->setRange(0,highestValue + 100);
    m_chart->addAxis(axisY, Qt::AlignLeft);
    plicuriSeries->attachAxis(axisY);
    colectaSeries->attachAxis(axisY);
}

void StatisticsComponent::clearChart() {
    auto axes = m_chart->axes();
    for (int i=0; i<axes.length(); ++i) m_chart->removeAxis(axes[i]);

    auto series = m_chart->series();
    for (int i=0; i<series.length(); ++i) m_chart->removeSeries(series[i]);
}
