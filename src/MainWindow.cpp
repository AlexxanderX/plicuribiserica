#include "MainWindow.hpp"

#include <QHBoxLayout>
#include "YesNoDialog.hpp"
#include <QMenu>
#include <QMenuBar>

#include "SettingsWindow.hpp"

#include <QSqlQuery>

#include <QDebug>

MainWindow::MainWindow(const std::shared_ptr<QSqlDatabase> db, Settings& settings, QWidget *parent)
    : QMainWindow { parent }
    , m_db { db }
    , m_settings { settings }
{
    m_dateSelectComponent = new DateSelectComponent(settings);
    m_dateViewComponent = new DateViewComponent(settings);
    m_statistiscsComponent = new StatisticsComponent(settings);

    connect(m_dateSelectComponent, &DateSelectComponent::dateChanged, [=](const QString& date){
        if (!m_dateViewComponent->isSaved()) {
            QPointer<YesNoDialog> dialog = new YesNoDialog("Doresti sa salvezi schimbarile?", this);
            dialog->setModal(true);
            dialog->show();

            connect(dialog, &YesNoDialog::finished, [=](int result){
                if (result == YesNoDialog::Accepted) {
                    if (m_dateViewComponent->save()) {
                        m_dateViewComponent->setDate(date);
                    }
                } else {
                    m_dateViewComponent->setDate(date);
                }
            });
        } else {
            m_dateViewComponent->setDate(date);
        }

        m_statistiscsComponent->setVisible(false);
        m_membersComponent->setVisible(false);
        m_dateViewComponent->setVisible(true);
    });

    connect(m_dateSelectComponent, &DateSelectComponent::monthSelected, [=](int month, int year){
        if (
            m_statistiscsComponent->isVisible() &&
            m_statistiscsComponent->isMonthlyStatistics() &&
            month == m_statistiscsComponent->getMonth() &&
            year == m_statistiscsComponent->getYear()
        ) {
            return;
        }

        m_statistiscsComponent->setDate(month, year);

        m_dateViewComponent->setVisible(false);
        m_membersComponent->setVisible(false);
        m_statistiscsComponent->setVisible(true);
    });

    connect(m_dateSelectComponent, &DateSelectComponent::yearSelected, [=](int year){
        if (
            m_statistiscsComponent->isVisible() &&
            !m_statistiscsComponent->isMonthlyStatistics() &&
            year == m_statistiscsComponent->getYear()
        ) {
            return;
        }

        m_statistiscsComponent->setDate(year);

        m_dateViewComponent->setVisible(false);
        m_membersComponent->setVisible(false);
        m_statistiscsComponent->setVisible(true);
    });

    m_statistiscsComponent->setVisible(false);

    QPointer<QMenuBar> menuBar = new QMenuBar;
    auto membersActions = menuBar->addAction("Membrii");

    auto settingsAction = menuBar->addAction("Setari");

    auto helpMenu = menuBar->addMenu("Ajutor");
    auto manualAction = helpMenu->addAction("Manual");
    auto aboutAction = helpMenu->addAction("Despre");

    this->setMenuBar(menuBar);

    m_dateSelectComponent->setDatabase(db);
    m_dateViewComponent->setDatabase(db);
    m_statistiscsComponent->setDatabase(db);

    QFile f("style.css");
    if (!f.open(QFile::ReadOnly | QFile::Text)) {
        // error
    }

    QTextStream in(&f);
    auto style = in.readAll();

    this->setStyleSheet(style);

    loadMembers();
    m_dateViewComponent->setMembersData(m_members, m_membersIDHash, m_membersNPHash, m_membersNameHash);

    m_membersComponent = new MembersWindow(m_members, m_membersIDHash, m_membersNPHash, m_membersNameHash);
    m_membersComponent->setDatabase(db);
    m_membersComponent->setVisible(false);

    QPointer<QHBoxLayout> layout = new QHBoxLayout;
    layout->addWidget(m_dateSelectComponent);
    layout->addWidget(m_dateViewComponent);
    layout->addWidget(m_statistiscsComponent);
    layout->addWidget(m_membersComponent);
    QPointer<QWidget> centralWidget = new QWidget;
    centralWidget->setLayout(layout);
    this->setCentralWidget(centralWidget);
    this->showMaximized();

    connect(membersActions, &QAction::triggered, [=](){
        m_statistiscsComponent->setVisible(false);
        m_dateViewComponent->setVisible(false);
        m_membersComponent->setVisible(true);
    });

    connect(settingsAction, &QAction::triggered, [=](){
        QPointer<SettingsWindow> dialog = new SettingsWindow(m_settings);
        dialog->show();
        dialog->resize(500, 500);
    });

    connect(manualAction, &QAction::triggered, [=](){

    });
}

void MainWindow::resizeEvent(QResizeEvent *) {
    m_dateSelectComponent->setFixedWidth(210);
}

void MainWindow::loadMembers() {
    QSqlQuery query("SELECT * FROM `Membrii`", *m_db);

    while (query.next()) {
        auto id = query.value(0).toInt();
        auto np = query.value(1).toString();
        auto name = query.value(2).toString();
        bool sters = query.value(3).toInt() == 0 ? false : true;

        const int index = m_members.size();

        m_members.push_back({id, np, name, sters});
        m_membersIDHash.insert(id, index);
        m_membersNPHash.insert(np, index);
        m_membersNameHash.insert(name, index);
    }
}
