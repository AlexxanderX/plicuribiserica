////////////////////////////////////////////////////////////
//
// PlicuriBiserca
// Copyright (C) 2014-2019 Prisacariu Alexandru (zalexxanderx@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////
#ifndef STATISTICSCOMPONENT_HPP
#define STATISTICSCOMPONENT_HPP

#include <memory>

#include <QWidget>
#include <QPointer>

#include <QBarSet>
#include <QChart>
#include <QComboBox>

#include <QSqlDatabase>

#include "Settings.hpp"

class StatisticsComponent : public QWidget
{
    Q_OBJECT
public:
    explicit StatisticsComponent(Settings& settings, QWidget *parent = nullptr);

    void setDatabase(const std::shared_ptr<QSqlDatabase> db);
    void setDate(int month, int year);
    void setDate(int year);

    bool isMonthlyStatistics() const { return m_isMonthlyStatistics; }
    int  getMonth() const { return m_month; }
    int  getYear() const { return m_year; }

private:
    void loadData(const QString& plicuriSql, const QString& colectaSql);
    QHash<QString, int> convertDatabaseMoney(const QString& s);
    void loadStatisticsForCurrency(const QString& currency);

    void clearChart();

private:
    Settings& m_settings;

    std::shared_ptr<QSqlDatabase> m_db;

    QPointer<QtCharts::QChart> m_chart = new QtCharts::QChart();

    QHash<QString, QPair<QHash<QString, int>, QHash<QString, int>>> m_values;
    bool m_isMonthlyStatistics = true;

    QPointer<QComboBox> m_currencyTypeCB = new QComboBox;

    int m_month;
    int m_year;
};

#endif // STATISTICSCOMPONENT_HPP
