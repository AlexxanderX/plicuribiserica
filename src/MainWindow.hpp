////////////////////////////////////////////////////////////
//
// PlicuriBiserca
// Copyright (C) 2014-2019 Prisacariu Alexandru (zalexxanderx@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////
#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QPointer>
#include <memory>
#include <QResizeEvent>

#include "DateSelectComponent.hpp"
#include "DateViewComponent.hpp"
#include "StatisticsComponent.hpp"
#include "MembersWindow.hpp"

#include "MemberData.hpp"
#include <QJsonObject>

#include "Settings.hpp"

class QSqlDatabase;

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(const std::shared_ptr<QSqlDatabase> db, Settings& settings, QWidget *parent = nullptr);

protected:
    void resizeEvent(QResizeEvent *event);

private:
    void loadMembers();

private:
    QPointer<DateSelectComponent> m_dateSelectComponent;
    QPointer<DateViewComponent>   m_dateViewComponent;
    QPointer<StatisticsComponent> m_statistiscsComponent;
    QPointer<MembersWindow>       m_membersComponent;

    std::shared_ptr<QSqlDatabase> m_db;

    QList<MemberData>   m_members;
    QHash<QString, int> m_membersNPHash;
    QHash<QString, int> m_membersNameHash;
    QHash<int, int>     m_membersIDHash;

    Settings& m_settings;
};

#endif // MAINWINDOW_HPP
