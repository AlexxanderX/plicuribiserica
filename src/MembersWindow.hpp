////////////////////////////////////////////////////////////
//
// PlicuriBiserca
// Copyright (C) 2014-2019 Prisacariu Alexandru (zalexxanderx@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////
#ifndef MEMBERSWINDOW_HPP
#define MEMBERSWINDOW_HPP

#include <QWidget>
#include <QTableWidget>

#include <QPointer>
#include "MemberData.hpp"

#include <memory>
#include <QSqlDatabase>

#include <QMetaObject>

class MembersWindow : public QWidget
{
    Q_OBJECT
public:
    explicit MembersWindow(QList<MemberData>& members, QHash<int, int>& membersIDsHash, QHash<QString, int>& membersNPsHash, QHash<QString, int>& membersNamesHash, QWidget *parent = nullptr);

    void setDatabase(const std::shared_ptr<QSqlDatabase> db);

private:
    void search(const QString& text, int field);
    void populateWithAllMembers();
    void addRow(int rowIndex, int memberIndex);

    void connectTableCellChanged();

private:
    std::shared_ptr<QSqlDatabase> m_db;

    QList<MemberData>& m_members;
    QHash<int, int>& m_membersIDsHash;
    QHash<QString, int>& m_membersNPsHash;
    QHash<QString, int>& m_membersNamesHash;

    QPointer<QTableWidget> m_table = new QTableWidget;

    QMetaObject::Connection m_tableCellChangedConnection;
};

#endif // MEMBERSWINDOW_HPP
