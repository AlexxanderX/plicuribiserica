#include "WelcomeWindow.hpp"

#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include "SettingsWindow.hpp"
#include <QFormLayout>
#include <QLineEdit>

#include <QDesktopServices>
#include <QUrl>
#include <QCoreApplication>
#include <QDir>

#include <QPointer>

WelcomeWindow::WelcomeWindow(Settings& settings, QWidget *parent)
    : QDialog(parent)
    , m_settings { settings }
{
    QPointer<QLabel> titleLabel = new QLabel("PlicuriBiserica");
    titleLabel->setAlignment(Qt::AlignCenter);
    titleLabel->setStyleSheet("font-size: 30px;");

    QPointer<QLabel> descriptionLabel = new QLabel(
        "Multumim ca folositi PlicuriBiserica. Te rog sa citesti manualul pentru a stii cum sa folosesti acest program. Acesta va fi disponibil si din bara de sus, la Ajutor, sau din folderul unde a fost instalat acest program"
    );
    descriptionLabel->setWordWrap(true);
    descriptionLabel->setAlignment(Qt::AlignCenter);

    QPointer<QLabel> helpLabel = new QLabel("Pentru mai multe informatii pentru a completa aceste campuri te rog sa vezi manualul");
    helpLabel->setWordWrap(true);
    helpLabel->setAlignment(Qt::AlignCenter);

    QPointer<QLineEdit> bisericaLE = new QLineEdit();
    QPointer<QLineEdit> locatieLE = new QLineEdit();

    QPointer<QFormLayout> userInfoLayout = new QFormLayout;
    userInfoLayout->addRow("Biserica", bisericaLE);
    userInfoLayout->addRow("Locatie", locatieLE);

    QPointer<SettingsWindow> settingsComponent = new SettingsWindow(settings, false);

    QPointer<QPushButton> manualButton = new QPushButton("Manual");
    QPointer<QPushButton> continueButton = new QPushButton("Continua");

    QPointer<QHBoxLayout> buttonsLayout = new QHBoxLayout;
    buttonsLayout->addWidget(manualButton);
    buttonsLayout->addWidget(continueButton);

    QPointer<QVBoxLayout> mainLayout = new QVBoxLayout;
    mainLayout->addWidget(titleLabel);
    mainLayout->addWidget(descriptionLabel);
    mainLayout->addLayout(userInfoLayout);
    mainLayout->addWidget(helpLabel);
    mainLayout->addWidget(settingsComponent);
    mainLayout->addLayout(buttonsLayout);
    this->setLayout(mainLayout);

    connect(manualButton, &QPushButton::clicked, [=](){
        QDesktopServices::openUrl(QUrl::fromLocalFile(QDir::currentPath() + QDir::separator() + "Documentatie.pdf"));
    });

    connect(continueButton, &QPushButton::clicked, [=](){
         if (settingsComponent->save()) accept();
    });
}
