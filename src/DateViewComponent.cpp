#include "DateViewComponent.hpp"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QMenu>
#include <QWidgetAction>
#include <QComboBox>
#include "YesNoDialog.hpp"
#include <QMessageBox>
#include <QTextDocument>
#include <QPrinter>
#include <QPrinterInfo>
#include <QPrintPreviewDialog>
#include <QDir>
#include <QFileDialog>
#include <QInputDialog>

#include <QStringListModel>
#include <QSqlQuery>
#include <QSqlError>
#include <QTextStream>
#include <QRegularExpression>
#include <QRegExpValidator>
#include <QRegExp>

#include <QJsonArray>

#include <QDebug>

const QString     DateViewComponent::newLineHTML = "<br style=\"page-break-before: always\">";

DateViewComponent::DateViewComponent(Settings& settings, QWidget *parent)
    : QWidget(parent)
    , m_settings { settings }
{
    m_defaultRowData = QStringList() << "" << "" << "" << m_settings.get("romanianCurrency").toString();

    QString acceptedCurrenciesRegExp = "(";
    for (const auto& currencyJSON : m_settings.get("currencies").toArray().toVariantList()) {
        acceptedCurrenciesRegExp += currencyJSON.toString() + "|";
    }

    acceptedCurrenciesRegExp.chop(1);
    acceptedCurrenciesRegExp += ")";

    m_sumaTypeValidator.setRegExp(QRegExp(acceptedCurrenciesRegExp, Qt::CaseInsensitive));

    m_table->setColumnCount(5);
    m_table->setHorizontalHeaderLabels(QStringList() << "NP" << "Nume" << "Suma" << "Valuta" << "Sterge");

    m_baniColectaLE->setValidator(new QDoubleValidator(0, 999999, 2));

    QPointer<QHBoxLayout> leiBaniColectaLayout = new QHBoxLayout;
    leiBaniColectaLayout->addWidget(m_baniColectaLE);
    leiBaniColectaLayout->addWidget(new QLabel(m_settings.get("romanianCurrency").toString()));

    m_baniColectaLayout->addRow(new QLabel("Bani colecta:"), leiBaniColectaLayout);

    QPointer<QHBoxLayout> inseratorLayout = new QHBoxLayout;
    inseratorLayout->addWidget(new QLabel("Numarator:"), 0, Qt::AlignRight);
    inseratorLayout->addWidget(m_inseratorLE);

    QPointer<QMenu> printMenu = new QMenu("");
    auto onlyChitanteColectaAction     = printMenu->addAction("Doar chitante colecta");
    auto onlyChitanteMembriiAction     = printMenu->addAction("Doar chitantele membrii");
    auto onlyTableAction               = printMenu->addAction("Doar tabelul");

    m_printButton->setPopupMode(QToolButton::MenuButtonPopup);
    m_printButton->setText("Printeaza tabel si chitante");
    m_printButton->setMenu(printMenu);
    m_printButton->setMaximumWidth(220);
    m_printButton->setEnabled(false);

    m_saveButton->setMaximumWidth(200);
    m_saveButton->setEnabled(false);

    m_revertButton->setMaximumWidth(200);
    m_revertButton->setEnabled(false);

    m_exportChitanteButton->setMaximumWidth(200);
    m_exportChitanteButton->setEnabled(false);

    QPointer<QHBoxLayout> buttonsLayout = new QHBoxLayout;
    buttonsLayout->addWidget(m_revertButton, 0 , Qt::AlignRight);
    buttonsLayout->addWidget(m_saveButton);
    buttonsLayout->addWidget(m_exportChitanteButton);
    buttonsLayout->addWidget(m_printButton);

    QPointer<QVBoxLayout> rightLayout = new QVBoxLayout;
    rightLayout->addLayout(inseratorLayout);
    rightLayout->addLayout(buttonsLayout);

    m_totalSum->setText("Total:\n");
    m_totalSum->setWordWrap(true);
    m_totalSum->setAlignment(Qt::AlignCenter);
    m_totalSum->setFixedWidth(150);

    QPointer<QHBoxLayout> bottomLayout = new QHBoxLayout;
    bottomLayout->addLayout(m_baniColectaLayout, 1);
    bottomLayout->addWidget(m_totalSum, 2);
    bottomLayout->addLayout(rightLayout, 1);

    QPointer<QVBoxLayout> layout = new QVBoxLayout;
    layout->addWidget(m_dateLabel);
    layout->addWidget(m_table);
    layout->addLayout(bottomLayout);
    this->setLayout(layout);

    m_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    m_table->horizontalHeader()->setSectionResizeMode(4, QHeaderView::ResizeToContents);
    m_table->horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeToContents);
    m_table->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);

    connect(m_saveButton, &QPushButton::clicked, std::bind(&DateViewComponent::save, this, CheckOptions::CheckTable | CheckOptions::CheckInserator));
    connect(m_revertButton, &QPushButton::clicked, [=](){
        setDate(m_date);
    });

    connect(m_baniColectaLE, &QLineEdit::textEdited, [=](){
        m_saveButton->setEnabled(true);
        m_revertButton->setEnabled(true);
    });

    connect(m_inseratorLE, &QLineEdit::textEdited, [=](){
        m_saveButton->setEnabled(true);
        m_revertButton->setEnabled(true);
    });

    connect(m_printButton, &QToolButton::clicked, std::bind(&DateViewComponent::print, this, PrintOptions::PrintAll, ""));

    auto onlyOneTypePrintFunc = [=](int options){
        auto location = QFileDialog::getSaveFileName(this, "Selecteaza locatie salvare fisier", "", "Document (*.html *docx)");

        if (location.length()) {
            print(options, location);
        }
    };

    connect(onlyTableAction, &QAction::triggered, std::bind(onlyOneTypePrintFunc, PrintOptions::PrintTable));
    connect(onlyChitanteMembriiAction, &QAction::triggered, std::bind(onlyOneTypePrintFunc, PrintOptions::PrintChitanteMembrii));
    connect(onlyChitanteColectaAction, &QAction::triggered, std::bind(onlyOneTypePrintFunc, PrintOptions::PrintChitanteColecta));

    connect(m_baniColectaLE, &QLineEdit::textChanged, std::bind(&DateViewComponent::baniColectaLETextEdited, this, std::placeholders::_1, 0));

    DIGITS_M.insert(1, "UNU");
    DIGITS_M.insert(2, "DOI");
    DIGITS_M.insert(3, "TREI");
    DIGITS_M.insert(4, "PATRU");
    DIGITS_M.insert(5, "CINCI");
    DIGITS_M.insert(6, "SASE");
    DIGITS_M.insert(7, "SAPTE");
    DIGITS_M.insert(8, "OPT");
    DIGITS_M.insert(9, "NOUA");

    DIGITS_F.insert(1, "UNA");
    DIGITS_F.insert(2, "DOUA");
    DIGITS_F.insert(3, "TREI");
    DIGITS_F.insert(4, "PATRU");
    DIGITS_F.insert(5, "CINCI");
    DIGITS_F.insert(6, "SASE");
    DIGITS_F.insert(7, "SAPTE");
    DIGITS_F.insert(8, "OPT");
    DIGITS_F.insert(9, "NOUA");

    auto CompleterPopupItemClicked = [=](){
        int currentColumn = m_table->currentColumn();
        if (currentColumn == 0 || currentColumn == 1) {
            tableWidgetEditingFinished(m_table->currentRow(), currentColumn);
        }
    };

    connect(m_idsCompleter->popup(), &QAbstractItemView::clicked, CompleterPopupItemClicked);
    connect(m_namesCompleter->popup(), &QAbstractItemView::clicked, CompleterPopupItemClicked);
}

void DateViewComponent::setDatabase(const std::shared_ptr<QSqlDatabase> db) {
    m_db = db;
}

void DateViewComponent::setDate(const QString& date) {
    m_inseratorLE->setText("");
    m_baniColectaLE->setText("");

    m_sumsCurrencies.clear();

    qDebug() << "m_sumsCurrencies" << m_sumsCurrencies;

    while (m_baniColectaLayout->rowCount() != 1) {
        m_baniColectaLayout->removeRow(1);
    }

    m_table->setRowCount(0);

    m_date = date.mid(0,10);
    m_tableName = "t" + date.mid(6, 4);
    m_seria = date.mid(11, 1);

    m_dateLabel->setText("Data: " + m_date);

    QSqlQuery query("SELECT * FROM `" + m_tableName + "` WHERE `Data`=\"" + m_date + "\" AND `Seria`=" + m_seria, *m_db);

    while (query.next()) {
        auto memberID = query.value(1).toInt();

        auto memberIDIt = m_membersIDsHash->find(memberID);
        if (memberIDIt == m_membersIDsHash->end()) {
            // Error
        }

        auto member = m_members->at(memberIDIt.value());

        auto suma = query.value(2).toString().split(" ");
        auto data = QStringList() << member.np << member.name << suma[0] << suma[1];
        addRow(data);

        if (!m_inseratorLE->text().length()) m_inseratorLE->setText(query.value(4).toString());
    }

    addRow(m_defaultRowData);

    query.exec("SELECT `Suma` FROM `BaniColecta` WHERE Data=\"" + m_date + "\" AND `Seria`=" + m_seria);
    if (query.next()) {
        auto bani = query.value(0).toString().split(";");

        m_baniColectaLE->setText(bani[0].split(" ")[0]);

        for (int i=1; i < bani.size(); ++i) {
            addRowToBaniColecta(i);

            auto values = bani[i].split(" ");

            auto layout = m_baniColectaLayout->itemAt(i, QFormLayout::FieldRole)->layout();
            static_cast<QLineEdit*>(layout->itemAt(0)->widget())->setText(values[0]);
            static_cast<QLineEdit*>(layout->itemAt(1)->widget())->setText(values[1]);
        }
    } else {
        // Error
    }

    updateSumCurrencies();

    m_exportChitanteButton->setEnabled(true);
    m_printButton->setEnabled(true);
    m_revertButton->setEnabled(false);
    m_saveButton->setEnabled(false);
}

void DateViewComponent::setMembersData(QList<MemberData>& members, QHash<int, int>& membersIDsHash, QHash<QString, int>& membersNPsHash, QHash<QString, int>& membersNamesHash) {
    m_members = &members;
    m_membersIDsHash = &membersIDsHash;
    m_membersNPsHash = &membersNPsHash;
    m_membersNamesHash = &membersNamesHash;

    QStringList npsModelList;
    QStringList namesModelList;
    for (int i=0; i < members.size(); ++i) {
        if (members[i].sters) continue;

        npsModelList.append(members[i].np);
        namesModelList.append(members[i].name);
    }

    QPointer<QStringListModel> npsModel = new QStringListModel(npsModelList);
    QPointer<QStringListModel> namesModel = new QStringListModel(namesModelList);

    m_idsCompleter->setModel(npsModel);
    m_namesCompleter->setModel(namesModel);
}

bool DateViewComponent::save(int checkOptions) {
    auto error = isAllGood(checkOptions);
    if (error.length()) {
        QMessageBox::critical(this, "Eroare", error);
        return false;
    }

    //= Inserts new members/Change members =============================================================================
    if (m_newMembers.length() || m_changedMembersHash.size()) {
        for (const auto& rowIndex : m_newMembers) {
            auto name = static_cast<QLineEdit*>(m_table->cellWidget(rowIndex,1))->text();
            auto np   = static_cast<QLineEdit*>(m_table->cellWidget(rowIndex,0))->text();

            QSqlQuery query("INSERT INTO `Membrii`(`NP`,`Nume`) VALUES (\"" + np + "\",\"" + name + "\")", *m_db);

            MemberData member(query.lastInsertId().toInt(), np, name);

            const int index = m_members->size();
            m_members->push_back(member);
            m_membersNPsHash->insert(member.np, index);
            m_membersNamesHash->insert(member.name, index);
        }

        m_db->transaction();

        for (const auto& member : m_changedMembersHash) {
            m_db->exec("UPDATE `Membrii` SET `NP`=\"" + member.np + "\", `Nume`=\"" + member.name + "\" WHERE `ID`=" + QString::number(member.id));

            m_members->replace(m_membersIDsHash->value(member.id), member);
        }

        bool result = m_db->commit();

        if (!result) {
            QMessageBox::critical(this, "Eroare", m_db->lastError().text());
        }
    }
    //==================================================================================================================

    //= Add the table to the DB ========================================================================================
    m_db->transaction();

    m_db->exec("DELETE FROM `" + m_tableName + "` WHERE `Data`=\"" + m_date + "\" AND `Seria`=" + m_seria);

    const QString& inserator = m_inseratorLE->text();
    for (int i=0; i < m_table->rowCount() - 1; ++i) {
        auto suma     = static_cast<QLineEdit*>(m_table->cellWidget(i,2))->text();
        auto sumaType = static_cast<QLineEdit*>(m_table->cellWidget(i,3))->text().toUpper();

        auto np = static_cast<QLineEdit*>(m_table->cellWidget(i,0))->text();
        int  id = m_members->at(m_membersNPsHash->value(np)).id;

        m_db->exec("INSERT INTO `" + m_tableName + "`(`MembruID`,`Suma`,`Data`,`Inserator`,`Seria`) VALUES (" + QString::number(id) + ",\"" + suma + " " + sumaType + "\",\"" + m_date + "\",\"" + inserator + "\"," + m_seria + ")");
    }

    if (m_baniColectaLE->text().length()) {
        QString colectaValue = m_baniColectaLE->text() + " " + m_settings.get("romanianCurrency").toString();

        for (int i=1; i < m_baniColectaLayout->rowCount() - 1; ++i) {
            auto layout = m_baniColectaLayout->itemAt(i, QFormLayout::FieldRole)->layout();
            auto value = static_cast<QLineEdit*>(layout->itemAt(0)->widget())->text();

            if (value.length() && value.toInt()) {
                auto currency = static_cast<QLineEdit*>(layout->itemAt(1)->widget())->text();

                if (!currency.length()) {
                    // Error
                }

                colectaValue += ";" + value + " " + currency;
            }
        }

        m_db->exec("REPLACE INTO `BaniColecta` (`Data`,`Seria`,`Suma`) VALUES (\"" + m_date + "\", " + m_seria + ", \"" + colectaValue + "\")");
    }

    bool result = m_db->commit();

    if (!result) {
        QMessageBox::critical(this, "Eroare", m_db->lastError().text());
        return false;
    }
    //==================================================================================================================

    m_saveButton->setEnabled(false);
    m_revertButton->setEnabled(false);
    return true;
}

void DateViewComponent::resizeEvent(QResizeEvent *) {
    m_baniColectaLE->setFixedWidth(80);
    m_inseratorLE->setFixedWidth(300);
}

QString DateViewComponent::isAllGood(int options) {
    if (options & CheckOptions::CheckBaniColecta && !m_baniColectaLE->text().length()) {
        return "Nu ai completat suma bani colecta";
    }

    if (options & CheckOptions::CheckTable) {
        for (int i=0; i < m_table->rowCount() - 1; ++i) {
            auto sumaLE     = static_cast<QLineEdit*>(m_table->cellWidget(i,2));
            auto sumaTypeLE = static_cast<QLineEdit*>(m_table->cellWidget(i,3));
            auto nameLE     = static_cast<QLineEdit*>(m_table->cellWidget(i,1));
            auto npLE       = static_cast<QLineEdit*>(m_table->cellWidget(i,0));

            if (!sumaLE->text().length() || !sumaTypeLE->text().length() || !nameLE->text().length() || !npLE->text().length()) {
                return "Nu ai completat tot ce trebuie la randul " + QString::number(i+1);
            }
        }
    }

    if (options & CheckOptions::CheckInserator && !m_inseratorLE->text().length()) {
        return "Nu ai completat numele inseratorului";
    }

    return "";
}

void DateViewComponent::addRow(const QStringList &data) {
    const int rowIndex = m_table->rowCount();
    m_table->insertRow(rowIndex);

    QPointer<QLineEdit> npLE = new QLineEdit(data[0]);
    npLE->setCompleter(m_idsCompleter);
    m_table->setCellWidget(rowIndex, 0, npLE);

    QPointer<QLineEdit> nameLE = new QLineEdit(data[1]);
    nameLE->setCompleter(m_namesCompleter);
    m_table->setCellWidget(rowIndex, 1, nameLE);

    QPointer<QLineEdit> sumaLE = new QLineEdit(data[2]);
    sumaLE->setFixedWidth(100);
    sumaLE->setValidator(new QDoubleValidator(0, 999999, 2));
    m_table->setCellWidget(rowIndex, 2, sumaLE);

    QPointer<QLineEdit> sumaTypeLE = new QLineEdit(data[3]);
    sumaTypeLE->setValidator(&m_sumaTypeValidator);
    m_table->setCellWidget(rowIndex, 3, sumaTypeLE);

    QPointer<QPushButton> deleteButton = new QPushButton("X");
    deleteButton->setObjectName("XButton");
    deleteButton->setFixedWidth(50);
    m_table->setCellWidget(rowIndex, 4, deleteButton);

    m_baniColectaLE->setFixedWidth(80);

    connect(deleteButton, &QPushButton::clicked, std::bind(&DateViewComponent::tableWidgetXClicked, this, rowIndex));

    auto _tableWidgetTextChanged = std::bind(&DateViewComponent::tableWidgetTextChanged, this, rowIndex, std::placeholders::_1);
    connect(npLE, &QLineEdit::textEdited, _tableWidgetTextChanged);
    connect(nameLE, &QLineEdit::textEdited, _tableWidgetTextChanged);
    connect(sumaLE, &QLineEdit::textEdited, _tableWidgetTextChanged);

    connect(npLE, &QLineEdit::editingFinished, std::bind(&DateViewComponent::tableWidgetEditingFinished, this, rowIndex, 0));
    connect(nameLE, &QLineEdit::editingFinished, std::bind(&DateViewComponent::tableWidgetEditingFinished, this, rowIndex, 1));

    connect(sumaLE, &QLineEdit::editingFinished, [=](){ updateSumCurrencies(); });
    connect(sumaTypeLE, &QLineEdit::editingFinished, [=](){ updateSumCurrencies(); });

    connect(sumaTypeLE, &QLineEdit::textChanged, [=](const QString& text){ sumaTypeLE->setText(text.toUpper()); });
}

void DateViewComponent::addRowToBaniColecta(int row) {
    if (m_baniColectaLayout->rowCount() > row) return;

    QPointer<QLineEdit> sumaLE = new QLineEdit();
    sumaLE->setValidator(new QDoubleValidator(0, 999999, 2));
    sumaLE->setFixedWidth(80);

    QPointer<QLineEdit> sumaTypeLE = new QLineEdit;
    sumaTypeLE->setFixedWidth(60);
    sumaTypeLE->setValidator(&m_sumaTypeValidator);

    QPointer<QHBoxLayout> layout = new QHBoxLayout;
    layout->addWidget(sumaLE);
    layout->addWidget(sumaTypeLE);

    m_baniColectaLayout->addRow("", layout);

    connect(sumaTypeLE, &QLineEdit::textChanged, [=](const QString& text){ sumaTypeLE->setText(text.toUpper()); });
    connect(sumaLE, &QLineEdit::textChanged, std::bind(&DateViewComponent::baniColectaLETextEdited, this, std::placeholders::_1, row));
}

void DateViewComponent::tableWidgetTextChanged(int rowIndex, const QString& text) {
    m_textChanged = text;
    m_saveButton->setEnabled(true);
    m_revertButton->setEnabled(true);

    if (text.length()) {
        if (rowIndex == m_table->rowCount() - 1) {
            addRow(m_defaultRowData);
        }

    } else {
        if (rowIndex != m_table->rowCount() - 1) {
            // Check if the next rows are empty
            bool allEmpty = true;
            for (int i=rowIndex+1; i < m_table->rowCount(); ++i) {
                auto sumaLE     = static_cast<QLineEdit*>(m_table->cellWidget(i,2));
                auto sumaTypeLE = static_cast<QLineEdit*>(m_table->cellWidget(i,3));
                auto nameLE     = static_cast<QLineEdit*>(m_table->cellWidget(i,1));
                auto npLE       = static_cast<QLineEdit*>(m_table->cellWidget(i,0));

                if (sumaLE->text().length() || (sumaTypeLE->text().length() && sumaTypeLE->text() != m_settings.get("romanianCurrency").toString()) || nameLE->text().length() || npLE->text().length()) {
                    allEmpty = false;
                    break;
                }
            }

            if (allEmpty) {
                m_table->setRowCount(rowIndex+1);
            }
        }
    }
}

void DateViewComponent::tableWidgetEditingFinished(int rowIndex, int cellWidget) {
    auto ownLE = static_cast<QLineEdit*>(m_table->cellWidget(rowIndex, cellWidget));

    if (!m_textChanged.length()) return;

    auto otherLE   = static_cast<QLineEdit*>(m_table->cellWidget(rowIndex, cellWidget == 0 ? 1 : 0));
    auto otherHash = cellWidget == 0 ? m_membersNamesHash : m_membersNPsHash;
    auto ownHash   = cellWidget == 0 ? m_membersNPsHash : m_membersNamesHash;

    auto ownIt = ownHash->find(ownLE->text());

    // If the current editing LE has a value in the DB then replace the other LE with the equivalent
    if (ownIt != ownHash->end()) {
        // If it was in `m_newMembers` then delete it because is not new anymore
        m_newMembers.removeOne(rowIndex);

        const auto& member = m_members->at(ownIt.value());
        otherLE->setText(cellWidget == 0 ? member.name : member.np);
    } else {
        if (otherLE->text().length()) {
            auto otherIt = otherHash->find(otherLE->text());

            if (otherIt != otherHash->end()) {
                // If it was in `m_newMembers` then delete it because is not new anymore
                m_newMembers.removeOne(rowIndex);

                MemberData newMember;
                newMember.id = m_members->at(otherIt.value()).id;

                if (cellWidget == 0) {
                    newMember.np = ownLE->text();
                    newMember.name = otherLE->text();
                } else {
                    newMember.np = otherLE->text();
                    newMember.name = ownLE->text();
                }

                const auto& originalMember = m_members->at(otherIt.value());

                QPointer<YesNoDialog> dialog = new YesNoDialog("Doresti sa-l inlocuiesti pe \"" + originalMember.name + "\" - \"" + originalMember.np + "\" cu \"" + newMember.name + "\" - \"" + newMember.np + "\"?", this);
                dialog->setModal(true);
                dialog->show();

                // If the dialog was accepted then the user want to change that member so add it to the `m_changedMembersHash` hash for replacing on save
                connect(dialog, &YesNoDialog::accepted, [=](){
                    m_changedMembersHash.insert(rowIndex, newMember);
                });

                connect(dialog, &YesNoDialog::rejected, [=](){
                    ownLE->setText(cellWidget == 0 ? originalMember.np : originalMember.name);
                });
            } else {
                if (m_newMembers.indexOf(rowIndex) == -1) {
                    m_newMembers.push_back(rowIndex);
                }
            }
        }
    }

    m_textChanged = "";
}

void DateViewComponent::tableWidgetXClicked(int rowIndex) {
    // If is the last row then do nothing
    if (rowIndex == m_table->rowCount() - 1) return;

    QPointer<YesNoDialog> dialog = new YesNoDialog("Esti sigur ca vrei sa stergi?", this);
    dialog->setModal(true);
    dialog->show();

    connect(dialog, &YesNoDialog::accepted, [=](){
        m_table->removeRow(rowIndex);
        m_saveButton->setEnabled(true);
        m_revertButton->setEnabled(true);

        // Search if the row is on `m_changedMembersHash` hash and if is then delete the row
        m_changedMembersHash.remove(rowIndex);
        m_newMembers.removeOne(rowIndex);

        for (int i=rowIndex; i < m_table->rowCount(); ++i) {
            auto sumaLE     = static_cast<QLineEdit*>(m_table->cellWidget(i,2));
            auto nameLE     = static_cast<QLineEdit*>(m_table->cellWidget(i,1));
            auto npLE       = static_cast<QLineEdit*>(m_table->cellWidget(i,0));
            auto xButton    = static_cast<QPushButton*>(m_table->cellWidget(i,4));

            sumaLE->disconnect();
            nameLE->disconnect();
            npLE->disconnect();
            xButton->disconnect();

            auto _tableWidgetTextChanged = std::bind(&DateViewComponent::tableWidgetTextChanged, this, i, std::placeholders::_1);
            connect(npLE, &QLineEdit::textEdited, _tableWidgetTextChanged);
            connect(nameLE, &QLineEdit::textEdited, _tableWidgetTextChanged);
            connect(sumaLE, &QLineEdit::textEdited, _tableWidgetTextChanged);

            connect(npLE, &QLineEdit::editingFinished, std::bind(&DateViewComponent::tableWidgetEditingFinished, this, i, 0));
            connect(nameLE, &QLineEdit::editingFinished, std::bind(&DateViewComponent::tableWidgetEditingFinished, this, i, 1));

            connect(xButton, &QPushButton::clicked, std::bind(&DateViewComponent::tableWidgetXClicked, this, i));
        }
    });
}

QString DateViewComponent::getPrintTemplate(const QString& location) {
    QFile templateFile(location);

    if (!templateFile.open(QFile::ReadOnly)) {
        // ERROR
    }

    QString templateDoc = templateFile.readAll();
    templateDoc = templateDoc.mid(templateDoc.indexOf("<body>") + 6);
    templateDoc = templateDoc.mid(0, templateDoc.indexOf("</body>"));

    return templateDoc;
}

int DateViewComponent::getNrChitanta(bool* fromSettings) {
    bool dialogWasAccepted;
    int nrChitantaCurenta = QInputDialog::getInt(
                this,
                "Numar inceput chitanta",
                "Numar de inceput pentru prima chitanta. Lasa pe -1 pentru a continua cu urmatorul numar al chitantei automat.",
                -1,
                -1,
                2147483647,
                1,
                &dialogWasAccepted
    );

    if (!dialogWasAccepted) return -1;

    if (fromSettings) *fromSettings = true;
    nrChitantaCurenta =  m_settings.get("numarChitanta").toString().toInt();

    return nrChitantaCurenta;
}

void DateViewComponent::print(int options, const QString& location) {
    if (!save()) return;

    int copiesTabel = m_settings.get("copiiTabel").toString().toInt();
    int copiesChitanteMembrii = m_settings.get("copiiChitanteMembrii").toString().toInt();
    int copiesChitanteColecta = m_settings.get("copiiChitanteColecta").toString().toInt();

    QString fileLocation = location;
    if (!fileLocation.length()) {
        QString dateWithoutSlashes = m_date;
        dateWithoutSlashes.replace("/", "");

        fileLocation = m_settings.get("saveLocation").toString() + QDir::separator() + "Tabel" + dateWithoutSlashes + " " + m_seria + ".html";
    }

    QFile data(fileLocation);

    if (!data.open(QFile::WriteOnly | QFile::Truncate)) {
        QMessageBox::critical(this, "Eroare", "Nu se poate creea fisierul \"" + fileLocation + "\". Fie nu exista folderul fie este deschis deja fisierul in alt program(precum MSWord).");
        return;
    }

    QTextStream file(&data);
    file << "<html><head><style>#btable, #btable td {border: 1px solid black;	border-collapse: collapse;}</style></head><body>";

    if (options & PrintOptions::PrintTable) {
        printTable(file, options & PrintOptions::PrintAll ? copiesTabel : 1);
    }

    int nrChitantaCurenta = 1;
    bool needUpdateToNrChitante = false;

    if (options & PrintOptions::PrintChitanteMembrii || options & PrintOptions::PrintChitanteColecta) {
        nrChitantaCurenta = getNrChitanta(&needUpdateToNrChitante);

        if (nrChitantaCurenta == -1) return;
    }

    if (options & PrintOptions::PrintChitanteMembrii) {
        if (options & PrintOptions::PrintTable) {
            file << newLineHTML;
        }

        nrChitantaCurenta = printChitanteMembrii(file, nrChitantaCurenta, options & PrintOptions::PrintAll ? copiesChitanteMembrii : 1);
    }

    if (options & PrintOptions::PrintChitanteColecta) {
        if (options & PrintOptions::PrintTable || options & PrintOptions::PrintChitanteMembrii) {
            file << newLineHTML;
        }

        printChitanteColecta(file, nrChitantaCurenta, options & PrintOptions::PrintAll ? copiesChitanteColecta : 1);
    }

    file << "</body></html>";

    data.close();

    qDebug() << "SAVE" << (options & PrintOptions::PrintChitanteMembrii || options & PrintOptions::PrintChitanteColecta) << (needUpdateToNrChitante);

    if ((options & PrintOptions::PrintChitanteMembrii || options & PrintOptions::PrintChitanteColecta) && needUpdateToNrChitante) {
        m_settings.set("numarChitanta", QJsonValue(QString::number(nrChitantaCurenta)));
        m_settings.saveToFile();
    }

    QMessageBox::information(this, "Success", "Salvat cu success la \"" + fileLocation + "\"");
}

void DateViewComponent::printTable(QTextStream& file, int copies) {
    auto templateDoc = getPrintTemplate(m_settings.get("tableFile").toString());

    templateDoc.replace("[numarator]", m_inseratorLE->text());

    QString table;
    QTextStream tableStream(&table);

    updateSumCurrencies();

    tableStream << "<table id=\"btable\"><tr><td>Nr.</td><td>Nr. plic</td><td>Nume</td><td>Suma</td><td style='width:150px'>Semnatura</td>";
    for (int i=0; i<m_table->rowCount() - 1; ++i)
    {
        auto suma     = static_cast<QLineEdit*>(m_table->cellWidget(i,2))->text();
        auto sumaType = static_cast<QLineEdit*>(m_table->cellWidget(i,3))->text();
        auto name     = static_cast<QLineEdit*>(m_table->cellWidget(i,1))->text();
        auto np       = static_cast<QLineEdit*>(m_table->cellWidget(i,0))->text();

        tableStream << "<tr><td>" << i << "</td>";
        tableStream << "<td>" << np << "</td>";
        tableStream << "<td>" << name << "</td>";
        tableStream << "<td>" << suma + " " + sumaType << "</td>";
        tableStream << "<td>&nbsp;</td></tr>";
    }

    tableStream << "</table>";

    templateDoc.replace("[table]", table);

    QRegularExpression multiSyntaxRegExp("\\{(.*?)@(.*?)@(.*?)\\}");
    multiSyntaxRegExp.setPatternOptions(QRegularExpression::MultilineOption);

    auto match = multiSyntaxRegExp.match(templateDoc,0,QRegularExpression::PartialPreferFirstMatch);
    while (match.hasMatch()) {
        auto parts = match.captured().mid(1, match.captured().length()-2).split("@");

        if (parts[0] == "currencies") {
            QString f = "";

            QString p;
            auto keys = m_sumsCurrencies.keys();
            for (int i=0; i < keys.length(); ++i) {

                p = parts[1];
                p.replace("[currency]", keys[i]);
                p.replace("[suma]", QString::number(m_sumsCurrencies[keys[i]]));

                if (i != keys.length() - 1) {
                    p += parts[2];
                }

                f += p;
            }

            templateDoc.replace(match.capturedStart(), match.capturedLength(), f);
        }

        match = multiSyntaxRegExp.match(templateDoc);
    }

    file << templateDoc;

    if (copies > 1) {
        for (int i=1; i < copies; ++i) {
            file << newLineHTML;
            file << templateDoc;
        }
    }
}

int DateViewComponent::printChitanteMembrii(QTextStream &file, int nrChitantaStart, int copies) {
    auto templateDoc = getPrintTemplate(m_settings.get("chitantaFile").toString());

    int nrChitantaCurenta = nrChitantaStart;

    QString f = "";

    const int ChitantePerPagina = m_settings.get("chitantePerPagina").toString().toInt();

    for (int i=0; i<m_table->rowCount() - 1; ++i)
    {
        auto suma     = static_cast<QLineEdit*>(m_table->cellWidget(i,2))->text();
        auto sumaType = static_cast<QLineEdit*>(m_table->cellWidget(i,3))->text();
        auto name     = static_cast<QLineEdit*>(m_table->cellWidget(i,1))->text();
        auto np       = static_cast<QLineEdit*>(m_table->cellWidget(i,0))->text();

        QString chitanta = templateDoc;

        chitanta.replace("[antet]", m_settings.get("chitantaInformatii").toString().replace("\n", "<br/>"));
        chitanta.replace("[serie]", m_settings.get("numeSerie").toString());
        chitanta.replace("[nume]", name);
        chitanta.replace("[suma]", suma);
        chitanta.replace("[suma_type]", sumaType);
        chitanta.replace("[data]", m_date);
        chitanta.replace("[numar_chitanta]", QString::number(nrChitantaCurenta));
        chitanta.replace("[motiv]", m_settings.get("chitantaMembriiMotiv").toString());

        chitanta.replace("[suma_litere]", digitToLetters(suma.toInt(), suma.length()));

        f += chitanta;

        if ((i + 1) % ChitantePerPagina == 0) {
            f += newLineHTML;
        } else {
            f += "<br/><br/>";
        }

        ++nrChitantaCurenta;
    }

    file << f;

    if (copies > 1) {
        for (int i=1; i < copies; ++i) {
            file << newLineHTML;
            file << f;
        }
    }

    return nrChitantaCurenta;
}

int DateViewComponent::printChitanteColecta(QTextStream& file, int nrChitantaStart, int copies) {
    auto templateDoc = getPrintTemplate(m_settings.get("chitantaFile").toString());

    int nrChitantaCurenta = nrChitantaStart;

    QString f = "";

    const int ChitantePerPagina = m_settings.get("chitantePerPagina").toString().toInt();

    for (int i=0; i < m_baniColectaLayout->rowCount() - 1; ++i) {
        QString chitanta = templateDoc;

        auto layout = m_baniColectaLayout->itemAt(i, QFormLayout::FieldRole)->layout();

        auto suma = static_cast<QLineEdit*>(layout->itemAt(0)->widget())->text();

        QString sumaType;
        if (i == 0) sumaType = m_settings.get("romanianCurrency").toString();
        else {
            sumaType = static_cast<QLineEdit*>(layout->itemAt(1)->widget())->text();
        }

        chitanta.replace("[antet]", m_settings.get("chitantaInformatii").toString().replace("\n", "<br/>"));
        chitanta.replace("[serie]", m_settings.get("numeSerie").toString());
        chitanta.replace("[nume]", m_settings.get("chitantaColectaName").toString());
        chitanta.replace("[suma]", suma);
        chitanta.replace("[suma_type]", sumaType);
        chitanta.replace("[data]", m_date);
        chitanta.replace("[numar_chitanta]", QString::number(nrChitantaCurenta));
        chitanta.replace("[motiv]", m_settings.get("chitantaColectaMotiv").toString());

        chitanta.replace("[suma_litere]", digitToLetters(suma.toInt(), suma.length()));

        f += chitanta;

        if ((i + 1) % ChitantePerPagina == 0) {
            f += newLineHTML;
        } else {
            f += "<br/><br/>";
        }

        ++nrChitantaCurenta;
    }

    file << f;

    if (m_table->rowCount() % ChitantePerPagina != 0) {
        file << newLineHTML;
    }

    if (copies > 1) {
        for (int i=1; i < copies; ++i) {
            file << newLineHTML;
            file << f;
        }
    }

    return nrChitantaCurenta;
}

void DateViewComponent::updateSumCurrencies() {
    m_saveButton->setEnabled(true);
    m_revertButton->setEnabled(true);

    m_sumsCurrencies.clear();

    for (int i=0; i<m_table->rowCount() - 1; ++i)
    {
        auto suma     = static_cast<QLineEdit*>(m_table->cellWidget(i,2))->text();
        auto sumaType = static_cast<QLineEdit*>(m_table->cellWidget(i,3))->text();

        auto currencyIt = m_sumsCurrencies.find(sumaType);
        if (currencyIt != m_sumsCurrencies.end()) {
            m_sumsCurrencies.insert(sumaType, currencyIt.value() + suma.toInt());
        } else {
            m_sumsCurrencies.insert(sumaType, suma.toInt());
        }
    }

    QString text = "Total:\n";

    auto keys = m_sumsCurrencies.keys();
    for (const auto& currency : m_sumsCurrencies.keys()) {
        text += QString::number(m_sumsCurrencies[currency]) + " " + currency + "\n";
    }

    m_totalSum->setText(text);
}

QString DateViewComponent::digitToLetters(int number, int totalDigitsCount) {
    QString sumLetters;

    if (number == 10) sumLetters = ZECE;
    else if (number == 100) sumLetters = SUTA;
    else if (number == 1000) sumLetters = MIE;
    else if (number == 10000) sumLetters = ZECE + N1000;
    else if (number == 100000) sumLetters = SUTA + "DE" + N1000;

    if (!sumLetters.length()) {
        int digitCount = 0;
        while (number) {
            int digit = number % 10;
            ++digitCount;

            if (digitCount == 1 && digit != 0) {
                sumLetters.push_front(DIGITS_M[digit]);
            } else if (digitCount == 2) {
               if (digit == 1) {
                   sumLetters = sumLetters + "SPREZECE";
               } else {
                   sumLetters.push_front(DIGITS_F[digit] + N10 + (sumLetters.length() ? "SI" : ""));
               }
            } else if (digitCount == 3) {
                sumLetters.push_front(digit == 1 ? SUTA : DIGITS_F[digit] + N100);
            } else if (digitCount == 4) {
                if (totalDigitsCount == 4) {
                    sumLetters.push_front(digit == 1 ? MIE : DIGITS_F[digit] + N1000);
                } else {
                    sumLetters.push_front(digitToLetters(number, totalDigitsCount - digitCount));
                    return sumLetters;
                }
            }

            number /= 10;
        }
    }

    return sumLetters.toLower();
}

void DateViewComponent::baniColectaLETextEdited(const QString& text, int row) {
    m_saveButton->setEnabled(true);
    m_revertButton->setEnabled(true);

    if (text.length()) {
        if (row == m_baniColectaLayout->rowCount() - 1) {
            addRowToBaniColecta(row + 1);
        }
    } else {
        if (row != m_baniColectaLayout->rowCount() - 1) {
            bool nextRowsAreAllEmpty = true;
            for (int i=row+1; i < m_baniColectaLayout->rowCount(); ++i) {
                if (static_cast<QLineEdit*>(m_baniColectaLayout->itemAt(i, QFormLayout::FieldRole)->layout()->itemAt(0)->widget())->text().length()) {
                    nextRowsAreAllEmpty = false;
                    break;
                }
            }

            if (nextRowsAreAllEmpty) {
                while (m_baniColectaLayout->rowCount() - 1 != row) {
                    m_baniColectaLayout->removeRow(row + 1);
                }
            }
        }
    }
}

void DateViewComponent::updateNrChitanta(int newNr, QFile* nrChitantaFile) {
    QTextStream stream(nrChitantaFile);
    stream.seek(0);
    stream << newNr;
    nrChitantaFile->close();
}
