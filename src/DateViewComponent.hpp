////////////////////////////////////////////////////////////
//
// PlicuriBiserca
// Copyright (C) 2014-2019 Prisacariu Alexandru (zalexxanderx@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////
#ifndef DATEVIEWCOMPONENT_HPP
#define DATEVIEWCOMPONENT_HPP

#include <QWidget>
#include <QPointer>
#include "MyTableWidget.hpp"
#include <QLineEdit>
#include <QCompleter>
#include <QToolButton>
#include <QPushButton>
#include <QLabel>
#include <QFormLayout>

#include <QResizeEvent>

#include <memory>
#include <QSqlDatabase>
#include "MemberData.hpp"
#include "Settings.hpp"

class DateViewComponent : public QWidget
{
    Q_OBJECT
public:
    enum CheckOptions {
        CheckInserator   = 0x01,
        CheckBaniColecta = 0x02,
        CheckTable       = 0x04,

        CheckAll         = CheckInserator | CheckBaniColecta | CheckTable
    };

public:
    explicit DateViewComponent(Settings& settings, QWidget *parent = nullptr);

    void setDatabase(const std::shared_ptr<QSqlDatabase> db);
    void setDate(const QString& date);

    void setMembersData(QList<MemberData>& members, QHash<int, int>& membersIDsHash, QHash<QString, int>& membersNPsHash, QHash<QString, int>& membersNamesHash);

    bool isSaved() const { return !m_saveButton->isEnabled(); }

    bool save(int checkOptions = CheckOptions::CheckAll);
signals:

public slots:

protected:
    void resizeEvent(QResizeEvent *event);

private:
    enum PrintOptions {
        PrintTable           = 0x01,
        PrintChitanteMembrii = 0x02,
        PrintChitanteColecta = 0x04,

        PrintAll = PrintTable | PrintChitanteMembrii | PrintChitanteColecta
    };

    const QString UNU_F = "UNA";
    const QString DOI_F = "DOUA";
    const QString TREI_F = "TREI";
    const QString PATRU_F = "PATRU";
    const QString CINCI_F = "CINCI";
    const QString SASE_F = "SASE";
    const QString SAPTE_F = "SAPTE";
    const QString OPT_F = "OPT";
    const QString NOUA_F = "NOUA";

    QHash<int, QString> DIGITS_M;
    QHash<int, QString> DIGITS_F;

    const QString ZECE = "ZECE";
    const QString SUTA = "OSUTA";
    const QString MIE  = "OMIE";

    const QString N10    = "ZECI";
    const QString N100   = "SUTE";
    const QString N1000  = "MII";

private:
    QString isAllGood(int options = CheckOptions::CheckAll);

    void addRow(const QStringList& data);
    void addRowToBaniColecta(int row);

    void tableWidgetTextChanged(int rowIndex, const QString& text);
    void tableWidgetEditingFinished(int rowIndex, int cellWidget);
    void tableWidgetXClicked(int rowIndex);

    QString getPrintTemplate(const QString& location);
    int     getNrChitanta(bool* fromSettings);

    void print(int options = PrintOptions::PrintAll, const QString& location = "");
    void printTable(QTextStream& file, int copies = 1);
    int  printChitanteMembrii(QTextStream& file, int nrChitantaStart, int copies = 1);
    int  printChitanteColecta(QTextStream& file, int nrChitantaStart, int copies = 1);

    void updateSumCurrencies();

    QString digitToLetters(int number, int digitsCount);

    void baniColectaLETextEdited(const QString& text, int row);

    void updateNrChitanta(int newNr, QFile* nrChitantaFile);

private:
    Settings& m_settings;

    QPointer<QLabel>        m_dateLabel     = new QLabel("Data: nespecificata");
    QPointer<MyTableWidget> m_table         = new MyTableWidget;
    QPointer<QLineEdit>     m_baniColectaLE = new QLineEdit;
    QPointer<QLineEdit>     m_inseratorLE   = new QLineEdit;
    QPointer<QFormLayout>   m_baniColectaLayout = new QFormLayout;
    QPointer<QLabel>        m_totalSum          = new QLabel;

    QPointer<QToolButton> m_printButton = new QToolButton;
    QPointer<QPushButton> m_saveButton = new QPushButton("Salveaza schimbarile");
    QPointer<QPushButton> m_revertButton = new QPushButton("Anuleaza schimbarile");
    QPointer<QPushButton> m_exportChitanteButton = new QPushButton("Exporta chitantele");

    std::shared_ptr<QSqlDatabase> m_db;
    QString m_date;
    QString m_tableName;
    QString m_seria;

    QList<MemberData>*   m_members;
    QHash<int, int>*     m_membersIDsHash;
    QHash<QString, int>* m_membersNPsHash;
    QHash<QString, int>* m_membersNamesHash;

    QHash<QString, int> m_sumsCurrencies;

    QPointer<QCompleter> m_idsCompleter = new QCompleter;
    QPointer<QCompleter> m_namesCompleter = new QCompleter;

    QString m_textChanged = "";

    QHash<int, MemberData> m_changedMembersHash;
    QList<int> m_newMembers;

    QRegExpValidator m_sumaTypeValidator;

    QStringList m_defaultRowData;

    static const QString     newLineHTML;
};

#endif // DATEVIEWCOMPONENT_HPP
