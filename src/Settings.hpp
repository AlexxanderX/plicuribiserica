////////////////////////////////////////////////////////////
//
// PlicuriBiserca
// Copyright (C) 2014-2019 Prisacariu Alexandru (zalexxanderx@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////
#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include <QJsonObject>
#include <QHash>

class Settings
{
public:
    Settings(QJsonObject settings);

    QJsonValue get(const QString& name) const;
    void set(const QString& key, const QJsonValue& value);
    void saveToFile();

    QHash<QString, QString> labels();
    QJsonValue getLabel(const QString& name) const;
    QString type(const QString& name) const;
private:
    QJsonObject m_settings;
};

#endif // SETTINGS_HPP
