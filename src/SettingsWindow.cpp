#include "SettingsWindow.hpp"

#include <QFormLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QTextEdit>
#include <QFileDialog>

#include <QMessageBox>

#include <QDebug>

SettingsWindow::SettingsWindow(Settings& settings, bool showButtons, QWidget *parent)
    : QDialog(parent)
    , m_settings { settings }
{
    initialize();
    fillValuesFromConfig();

    QPointer<QVBoxLayout> mainLayout = new QVBoxLayout;
    mainLayout->addLayout(m_formLayout);

    this->setLayout(mainLayout);

    if (showButtons) {
        QPointer<QPushButton> resetButton = new QPushButton("Reseteaza");
        QPointer<QPushButton> saveButton = new QPushButton("Salveaza");

        mainLayout->addWidget(resetButton);
        mainLayout->addWidget(saveButton);

        connect(resetButton, &QPushButton::clicked, std::bind(&SettingsWindow::fillValuesFromConfig, this));
        connect(saveButton, &QPushButton::clicked, std::bind(&SettingsWindow::save, this));
    }

}

bool SettingsWindow::save() {
    for (const auto& key : m_settingsWidgets.keys()) {
        QString text;
        if (m_settings.type(key) == "text") {
            text = static_cast<QTextEdit*>(m_settingsWidgets[key])->toPlainText();
        } else {
            text = static_cast<QLineEdit*>(m_settingsWidgets[key])->text();
        }

        if (text.isEmpty()) {
            QMessageBox::critical(this, "Eroare", "Campul \"" + m_settings.getLabel(key).toString() + "\" nu poate fi gol");
            return false;
        }

        m_settings.set(key, text);
    }

    m_settings.saveToFile();

    QMessageBox::information(this, "Success", "Salvat cu success");
    return true;
}

void SettingsWindow::initialize() {
    auto labels = m_settings.labels();

    auto createFileFolderInput = [=](const QString& key, bool selectFile) -> QPointer<QHBoxLayout>{
        QPointer<QLineEdit> line = new QLineEdit;
        connect(line, &QLineEdit::textChanged, [=](const QString& text){
             m_settingsChanged[key] = text;
        });

        QPointer<QPushButton> searchBtn = new QPushButton("...");
        connect(searchBtn, &QPushButton::clicked, [=](){
            QString response;

            if (selectFile) {
                response = QFileDialog::getOpenFileName(this, "Selecteaza fisierul");
            } else {
                response = QFileDialog::getExistingDirectory(this, "Selecteaza folderul");
            }

            if (response.size() != 0) line->setText(response);
        });

        QPointer<QHBoxLayout> layout = new QHBoxLayout;
        layout->addWidget(line);
        layout->addWidget(searchBtn);

        m_settingsWidgets[key] = line;

        return layout;
    };

    auto keys = labels.keys();

    // Sort the keys
    qSort(keys.begin(), keys.end(), [](const QString& k1, const QString& k2)->bool{
        return k1.compare(k2) < 0;
    });

    for (const auto& key : keys) {
        auto type = m_settings.type(key);

        auto label = labels.value(key) + ":";

        if (type == "string" || type == "number") {
            QPointer<QLineEdit> line = new QLineEdit;
            connect(line, &QLineEdit::textChanged, [=](const QString& text){
                 m_settingsChanged[key] = text;
            });

            m_formLayout->addRow(label, line);

            m_settingsWidgets[key] = line;
        } else if (type == "text") {
            QPointer<QTextEdit> text = new QTextEdit;
            connect(text, &QTextEdit::textChanged, [=](){
                 m_settingsChanged[key] = text->toPlainText();
            });

            m_formLayout->addRow(label, text);

            m_settingsWidgets[key] = text;
        } else if (type == "file") {
            m_formLayout->addRow(label, createFileFolderInput(key, true));
        } else if (type == "folder") {
            m_formLayout->addRow(label, createFileFolderInput(key, false));
        }
    }
}

void SettingsWindow::fillValuesFromConfig() {
    for (const auto& key : m_settingsWidgets.keys()) {
        if (m_settings.type(key) == "text") {
            static_cast<QTextEdit*>(m_settingsWidgets[key])->setText(m_settings.get(key).toString());
        } else {
            static_cast<QLineEdit*>(m_settingsWidgets[key])->setText(m_settings.get(key).toString());
        }
    }
}
