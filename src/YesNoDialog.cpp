#include "YesNoDialog.hpp"

#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include <QPointer>

YesNoDialog::YesNoDialog(const QString& text, QWidget *parent) : QDialog(parent)
{
    QPointer<QPushButton> yesButton = new QPushButton("Da");
    QPointer<QPushButton> noButton = new QPushButton("Nu");
    noButton->setAutoDefault(true);
    noButton->setDefault(true);

    connect(yesButton, &QPushButton::clicked, [=](){ accept(); });
    connect(noButton, &QPushButton::clicked, [=](){ reject(); });

    QPointer<QHBoxLayout> buttonsLayout = new QHBoxLayout;
    buttonsLayout->addWidget(yesButton);
    buttonsLayout->addWidget(noButton);

    QPointer<QVBoxLayout> layout = new QVBoxLayout;
    layout->addWidget(new QLabel(text));
    layout->addLayout(buttonsLayout);

    this->setLayout(layout);
}
