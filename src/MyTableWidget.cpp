#include "MyTableWidget.hpp"

#include <QKeyEvent>

#include <QDebug>

MyTableWidget::MyTableWidget(QWidget *parent) : QTableWidget(parent)
{
    connect(this, &QTableWidget::currentCellChanged, [=](int, int column){
        nextFocusChild = column;
    });
}

bool MyTableWidget::focusNextPrevChild(bool next)
{
    // check if current column is the editable column
    int currentColumn = nextFocusChild + 1;

    ++nextFocusChild;
    if (nextFocusChild == this->columnCount() - 1) nextFocusChild = 0;

    qDebug() << "currentColumn" << currentColumn << (tabKeyNavigation() && currentColumn != 3);

    QKeyEvent event(QEvent::KeyPress, next ? Qt::Key_Tab : Qt::Key_Backtab, Qt::NoModifier);
    if (tabKeyNavigation() && currentColumn != this->columnCount() - 1)
    {
        // Qt::Key_Down instead of Qt::Key_Tab and Qt::Key_Up instead of Qt::Key_Backtab

        keyPressEvent(&event);
        if (event.isAccepted())
            return true;
    }

    keyPressEvent(&event);
    return QTableWidget::focusNextPrevChild(next);
}
