#include "MembersWindow.hpp"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QComboBox>
#include <QLineEdit>
#include <QHeaderView>
#include "YesNoDialog.hpp"
#include "MembersHistoryWindow.hpp"

#include <QSqlQuery>

#include <QDebug>

MembersWindow::MembersWindow(QList<MemberData>& members, QHash<int, int>& membersIDsHash, QHash<QString, int>& membersNPsHash, QHash<QString, int>& membersNamesHash, QWidget *parent)
    : QWidget(parent)
    , m_members { members }
    , m_membersIDsHash { membersIDsHash }
    , m_membersNPsHash { membersNPsHash }
    , m_membersNamesHash { membersNamesHash }
{
    auto searchLE = new QLineEdit();

    auto searchFieldCB = new QComboBox();
    searchFieldCB->addItem("NP");
    searchFieldCB->addItem("Nume");

    connect(searchLE, &QLineEdit::textChanged, [=](const QString& text){
        if (text.isEmpty()) populateWithAllMembers();
        else {
            search(text, searchFieldCB->currentIndex());
        }
    });

    connect(searchFieldCB, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index){
        if (searchLE->text().isEmpty()) return;

        search(searchLE->text(), index);
    });

    auto resetSearchButton = new QPushButton("Reseteaza");
    connect(resetSearchButton, &QPushButton::clicked, std::bind(&MembersWindow::populateWithAllMembers, this));

    auto searchLayout = new QHBoxLayout;
    searchLayout->addWidget(searchLE);
    searchLayout->addWidget(searchFieldCB);
    searchLayout->addWidget(resetSearchButton);

    m_table->setColumnCount(4);
    m_table->setHorizontalHeaderLabels(QStringList() << "NP" << "Nume" << "" << "");

    populateWithAllMembers();

    QPointer<QVBoxLayout> layout = new QVBoxLayout;
    layout->addLayout(searchLayout);
    layout->addWidget(m_table);
    this->setLayout(layout);

    m_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    m_table->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    m_table->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
    m_table->horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeToContents);
}

void MembersWindow::setDatabase(const std::shared_ptr<QSqlDatabase> db) {
    m_db = db;
}

void MembersWindow::search(const QString& text, int field) {
    m_table->disconnect(m_tableCellChangedConnection);

    m_table->setRowCount(m_members.length());

    auto lowerText = text.toLower();

    int currentRow = 0;
    for (int i=0; i < m_members.length(); ++i) {
        if (
            (field == 0 && m_members[i].np.toLower().indexOf(lowerText) == -1) ||
            (field == 1 && m_members[i].name.toLower().indexOf(lowerText) == -1) ||
            m_members[i].sters
        ) {
            continue;
        }

        addRow(currentRow, i);

        ++currentRow;
    }

    connectTableCellChanged();
    m_table->setRowCount(currentRow);
}

void MembersWindow::populateWithAllMembers() {
    m_table->disconnect(m_tableCellChangedConnection);

    if (m_table->rowCount() == m_members.length()) return;
    m_table->setVisible(false);

    for (int i=0; i < m_table->rowCount(); ++i) m_table->removeRow(i);

    m_table->setRowCount(m_members.length());

    int currentRow = 0;
    for (int i=0; i < m_members.length(); ++i) {
        if (m_members[i].sters) continue;

        addRow(currentRow, i);

        ++currentRow;
    }

    connectTableCellChanged();
    m_table->setVisible(true);
}

void MembersWindow::addRow(int rowIndex, int memberIndex) {
    auto NPColumn = new QTableWidgetItem(m_members[memberIndex].np);
    NPColumn->setFlags(NPColumn->flags() & ~Qt::ItemIsEditable);

    m_table->setItem(rowIndex, 0, NPColumn);
    m_table->setItem(rowIndex, 1, new QTableWidgetItem(m_members[memberIndex].name));

    QPointer<QPushButton> historyButton = new QPushButton("Istoric");
    m_table->setCellWidget(rowIndex, 2, historyButton);

    QPointer<QPushButton> deleteButton = new QPushButton("X");
    deleteButton->setObjectName("XButton");
    m_table->setCellWidget(rowIndex, 3, deleteButton);

    connect(historyButton, &QPushButton::clicked, [=](){
        QPointer<MembersHistoryWindow> historyDialog = new MembersHistoryWindow(m_members[memberIndex].id, m_db, this);
        historyDialog->exec();
    });

    connect(deleteButton, &QPushButton::clicked, [=](){
        QPointer<YesNoDialog> dialog = new YesNoDialog("Esti sigur ca vrei sa stergi membrul?", this);
        auto dialogResponse = dialog->exec();

        if (dialogResponse == QDialog::Rejected) return;

        QSqlQuery query(*m_db);
        query.prepare("UPDATE `Membrii` SET `Sters`=1 WHERE `NP`=:np");
        query.bindValue(":np", m_members[memberIndex].np);
        query.exec();

        m_membersNamesHash.remove(m_members[memberIndex].name);
        m_membersNPsHash.remove(m_members[memberIndex].np);
        m_membersIDsHash.remove(m_members[memberIndex].id);
        m_members[memberIndex].sters = true;
        m_table->removeRow(rowIndex);
    });
}

void MembersWindow::connectTableCellChanged() {
    int lastRowChanged = -1;
    int lastColumnChanged = -1;

    m_tableCellChangedConnection = connect(m_table, &QTableWidget::cellChanged, [this, &lastRowChanged, &lastColumnChanged](int row, int column){
        if (lastRowChanged == row && lastColumnChanged == column) {
            lastRowChanged = -1;
            lastColumnChanged = -1;
            return;
        }

        lastRowChanged = row;
        lastColumnChanged = column;

        auto numeText = m_table->item(row, column)->text();

        auto np = m_table->item(row, 0)->text();
        auto membersIndex = m_membersNPsHash[np];

        if (numeText.isEmpty()) {
            m_table->item(row, column)->setText(m_members[membersIndex].name);
        } else {
            auto member = m_members.value(membersIndex);
            auto oldName = member.name;

            QPointer<YesNoDialog> dialog = new YesNoDialog("Esti sigur ca vrei sa schimbi din \"" + oldName + "\" in \"" + numeText + "\"", this);
            auto dialogResponse = dialog->exec();

            if (dialogResponse == QDialog::Rejected) {
                m_table->item(row, column)->setText(oldName);
                return;
            }

            QSqlQuery query(*m_db);
            query.prepare("UPDATE `Membrii` SET `Nume`=:nume WHERE `NP`=:np");
            query.bindValue(":nume", numeText);
            query.bindValue(":np", np);
            query.exec();

            member.name = numeText;
            m_members.replace(membersIndex, member);

            m_membersNamesHash.remove(oldName);
            m_membersNamesHash.insert(numeText, membersIndex);

            lastRowChanged = -1;
            lastColumnChanged = -1;
        }
    });
}
