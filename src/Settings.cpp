#include "Settings.hpp"

#include <QJsonDocument>
#include <QFile>

#include <QDebug>

Settings::Settings(QJsonObject settings)
    : m_settings { settings }
{

}

QJsonValue Settings::get(const QString& name) const {
    auto value = m_settings.value(name);
    if (value.isObject()) return value.toObject().value("value");
    else                  return value;
}

void Settings::set(const QString& key, const QJsonValue& value) {
    if (m_settings[key].isObject()) {
        auto obj = m_settings[key].toObject();
        obj["value"] = value;
        m_settings[key] = obj;
    } else {
        m_settings[key] = value;
    }
}

void Settings::saveToFile() {
    auto doc = QJsonDocument(m_settings);

    QFile file("settings.json");
    if (file.open(QFile::WriteOnly)) {
        file.write(doc.toJson());
    } else {
        // Error
    }
}

QHash<QString, QString> Settings::labels() {
    QHash<QString, QString> labels;

    for (const auto& key : m_settings.keys()) {
        if (m_settings.value(key).isObject()) {
            labels.insert(key, m_settings.value(key).toObject().value("label").toString());
        }
    }

    return labels;
}

QJsonValue Settings::getLabel(const QString& name) const {
    auto value = m_settings.value(name);
    if (value.isObject()) return value.toObject().value("label");
    else                  return value;
}

QString Settings::type(const QString& name) const {
    auto value = m_settings.value(name);
    if (value.isObject()) return value.toObject().value("type").toString();
    else                  return "";
}
