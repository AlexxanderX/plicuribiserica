#include "MembersHistoryWindow.hpp"

#include <QVBoxLayout>
#include <QTableWidget>
#include <QHeaderView>

#include <QSqlQuery>
#include <QList>

#include <QDebug>

struct Row {
    QString data;
    QString suma;
    QString inserator;
};

MembersHistoryWindow::MembersHistoryWindow(int memberID, const std::shared_ptr<QSqlDatabase> db, QWidget *parent) : QDialog(parent)
{
    QList<Row> rows;

    auto tables = db->tables();

    for (const auto& table : tables) {
        QSqlQuery query("SELECT * FROM `" + table + "` WHERE `MembruID`=" + QString::number(memberID), *db);

        while (query.next()) {
            Row row;
            row.data = query.value(3).toString();
            row.suma = query.value(2).toString();
            row.inserator = query.value(4).toString();

            rows.append(row);
        }
    }

    qSort(rows.begin(), rows.end(), [](const Row& r1, const Row& r2)->bool{
        const int r1Year = r1.data.mid(6,4).toInt();
        const int r2Year = r2.data.mid(6,4).toInt();

        if (r1Year == r2Year) {
            const int r1Month = r1.data.mid(3,2).toInt();
            const int r2Month = r2.data.mid(3,2).toInt();

            if (r1Month == r2Month) {
                const int r1Day = r1.data.mid(0,2).toInt();
                const int r2Day = r2.data.mid(0,2).toInt();

                return r1Day < r2Day;
            } else return r1Month < r2Month;
        }

        return r1Year < r2Year;
    });

    auto tableWidget = new QTableWidget;
    tableWidget->setColumnCount(3);
    tableWidget->setHorizontalHeaderLabels(QStringList() << "Data" << "Suma" << "Inserator");

    for (int i=0; i < rows.size(); ++i) {
        tableWidget->insertRow(tableWidget->rowCount());

        tableWidget->setItem(i, 0, new QTableWidgetItem(rows[i].data));
        tableWidget->setItem(i, 1, new QTableWidgetItem(rows[i].suma));
        tableWidget->setItem(i, 2, new QTableWidgetItem(rows[i].inserator));
    }

    auto mainLayout = new QVBoxLayout;
    mainLayout->addWidget(tableWidget);

    this->setLayout(mainLayout);

    this->setMinimumSize(600, 500);

    tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}
