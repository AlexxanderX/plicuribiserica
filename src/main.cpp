#include "MainWindow.hpp"

#include <QSqlDatabase>
#include <QApplication>
#include <QDir>
#include <QFile>
#include <QMessageBox>
#include <QFileDialog>

#include <memory>
#include <QJsonDocument>
#include <QJsonObject>
#include <QSqlQuery>
#include <QSqlError>

#include <QDebug>

#include "Settings.hpp"
#include "WelcomeWindow.hpp"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QFile settingsFile("settings.json");
    if (!settingsFile.open(QFile::ReadOnly)) {
        QMessageBox::critical(nullptr, "Eroare", "Fisierul de setari nu exista sau este corupt. Va rog contactati developer-ul");
        return a.exec();
    }

    QJsonObject settingsJSON = QJsonDocument::fromJson(settingsFile.readAll()).object();
    Settings settings(settingsJSON);

    const bool firstTime = settings.get("firstTime").toBool();

    auto dbPath = settingsJSON["db"].toObject()["value"].toString();
    auto db = std::shared_ptr<QSqlDatabase>(new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE")));

    if (!QFile::exists(dbPath)) {
        if (firstTime) {
            db->setDatabaseName(dbPath);
            bool ok = db->open();

            db->transaction();
            auto queries = settingsJSON["createDatabaseQuery"].toString().split(";");
            for (const auto& query : queries) db->exec(query);
            db->commit();

            settings.set("firstTime", QJsonValue(false));
            settings.saveToFile();
        } else {
            QMessageBox::critical(nullptr, "Eroare", "Baza de date nu a fost gasita. Dati ok si veti putea alege locatia bazei de date. Daca nu stiti despre ce vorbiti va rog sa contactati developerul");
            dbPath = QFileDialog::getOpenFileName(nullptr, "Selectare baza de date", a.applicationDirPath(), "Database (*.sqlite)");

            if (dbPath.length() == 0) {
                QMessageBox::critical(nullptr, "Eroare", "Nu ai selectat nimic, va rog sa contactazi developerul sau sa selectazi baza de date");
                return -1;
            }
        }
    } else {
        db->setDatabaseName(dbPath);
    }

    if (!db->isOpen()) {
        bool ok = db->open();

        if (!ok) {
            QMessageBox::critical(nullptr, "Eroare", "Nu am putut sa ma conectez la baza de date. Te rog sa mai incerci si daca nu sa contactezi developerul");
            return -1;
        }
    }

    MainWindow w(db, settings);
    w.show();

    if (firstTime) {
        auto welcomeDialog = new WelcomeWindow(settings, &w);
        int result = welcomeDialog->exec();

        if (result == QDialog::Rejected) {
            return 0;
        }
    }

    return a.exec();
}
