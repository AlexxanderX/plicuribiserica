#ifndef MEMBERSHISTORYWINDOW_HPP
#define MEMBERSHISTORYWINDOW_HPP

#include <QDialog>

#include <memory>
#include <QSqlDatabase>

class MembersHistoryWindow : public QDialog
{
    Q_OBJECT
public:
    explicit MembersHistoryWindow(int memberID, const std::shared_ptr<QSqlDatabase> db, QWidget *parent = nullptr);

signals:

public slots:
};

#endif // MEMBERSHISTORYWINDOW_HPP
