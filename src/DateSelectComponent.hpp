////////////////////////////////////////////////////////////
//
// PlicuriBiserca
// Copyright (C) 2014-2019 Prisacariu Alexandru (zalexxanderx@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////
#ifndef MONTHCOMPONENT_HPP
#define MONTHCOMPONENT_HPP

#include <QWidget>
#include <QPointer>
#include <QTreeWidget>
#include <QCheckBox>
#include <QDialog>

#include <memory>
#include <QSqlDatabase>

#include "Settings.hpp"

class DateSelectComponent : public QWidget
{
    Q_OBJECT
public:
    explicit DateSelectComponent(Settings& settings, QWidget *parent = nullptr);

    void setDatabase(const std::shared_ptr<QSqlDatabase> db);

signals:
    void dateChanged(const QString& date);
    void monthSelected(int month, int year);
    void yearSelected(int year);

public slots:

private:
    class NewDateDialog : public QDialog {
    public:
        explicit NewDateDialog(QWidget *parent = nullptr);

        QString getDate() const { return m_date; }
    private:
        QString m_date;
    };

private:
    void initialize(bool getAllYears);

    QString getMonth(int monthNo);
    int     getMonth(const QString& month);

private:
    QPointer<QTreeWidget> m_list = new QTreeWidget;
    QPointer<QCheckBox> m_showAllCheckbox;

    std::shared_ptr<QSqlDatabase> m_db;

    QStringList m_years;
};

#endif // MONTHCOMPONENT_HPP
