#include "DateSelectComponent.hpp"

#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QMessageBox>

#include <QRegExp>
#include <QSqlQuery>
#include <QHash>
#include <algorithm>
#include <QDate>
#include <QRandomGenerator>

#include "NoSortTreeWidgetItem.hpp"

#include <QDebug>

DateSelectComponent::NewDateDialog::NewDateDialog(QWidget *parent): QDialog (parent)
{
    QPointer<QLineEdit> dayLE = new QLineEdit;
    QPointer<QLineEdit> monthLE = new QLineEdit;
    QPointer<QLineEdit> yearLE = new QLineEdit;

    dayLE->setValidator(new QIntValidator(0, 31));
    monthLE->setValidator(new QIntValidator(0, 12));
    yearLE->setValidator(new QIntValidator(2000, 20000));

    auto date = QDate::currentDate();

    dayLE->setText(QString::number(date.day()));
    monthLE->setText(QString::number(date.month()));
    yearLE->setText(QString::number(date.year()));

    QPointer<QHBoxLayout> dateLayout = new QHBoxLayout;
    dateLayout->addWidget(dayLE);
    dateLayout->addWidget(new QLabel("/"));
    dateLayout->addWidget(monthLE);
    dateLayout->addWidget(new QLabel("/"));
    dateLayout->addWidget(yearLE);

    QPointer<QPushButton> createButton = new QPushButton("Creeaza");
    QPointer<QPushButton> cancelButton = new QPushButton("Renunta");

    connect(createButton, &QPushButton::clicked, [=](){
        int year = yearLE->text().toInt();
        int month = monthLE->text().toInt();
        int day = dayLE->text().toInt();

        if (QDate::isValid(year, month, day)) {
            m_date = ((day < 9) ? "0" + QString::number(day) : QString::number(day)) + "/" + ((month < 9) ? "0" + QString::number(month) : QString::number(month)) + "/" + QString::number(year);
            accept();
        } else {
            QMessageBox::critical(this, "Eroare", "Data nu este valida");
        }
    });
    connect(cancelButton, &QPushButton::clicked, [=](){ reject(); });

    QPointer<QHBoxLayout> buttonsLayout = new QHBoxLayout;
    buttonsLayout->addWidget(createButton);
    buttonsLayout->addWidget(cancelButton);

    QPointer<QVBoxLayout> layout = new QVBoxLayout;
    layout->addLayout(dateLayout);
    layout->addLayout(buttonsLayout);
    this->setLayout(layout);

    connect(dayLE, &QLineEdit::textChanged, [=](const QString& text){
        if (text.length() == 2) {
            monthLE->setFocus();
            monthLE->selectAll();
        }
    });

    connect(monthLE, &QLineEdit::textChanged, [=](const QString& text){
        if (text.length() == 2) {
            yearLE->setFocus();
            yearLE->selectAll();
        }
    });

    dayLE->setFocus();
}

DateSelectComponent::DateSelectComponent(Settings& settings, QWidget *parent) : QWidget(parent)
{
    m_list->setColumnCount(1);
    m_list->setHeaderHidden(true);

    connect(m_list, &QTreeWidget::itemClicked, [=](QTreeWidgetItem *item, int){
        if (QRegExp("[0-9]{2}.[0-9]{2}.[0-9]{4} [0-9]").exactMatch(item->text(0))) {
            emit dateChanged(item->text(0));
        } else if (!QRegExp("[0-9][0-9][0-9][0-9]").exactMatch(item->text(0))) {
            emit monthSelected(getMonth(item->text(0)), item->parent()->text(0).toInt());
        } else {
            emit yearSelected(item->text(0).toInt());
        }
    });

    QPointer<QPushButton> addMonthBtn = new QPushButton("Adauga data");

    connect(addMonthBtn, &QPushButton::clicked, [=](){
        QPointer<NewDateDialog> dialog = new NewDateDialog(this);
        dialog->setModal(true);
        dialog->show();

        connect(dialog, &NewDateDialog::accepted, [=](){
            auto date = dialog->getDate();

            auto year = date.mid(6,4);

            // Check year exists
            int seria = 1;
            if (m_years.contains(year)) {
                qDebug() << "GOOD";
                // Get seria
                QSqlQuery query("SELECT Seria FROM `t" + year + "` WHERE `Data`=\"" + date + "\" ORDER BY Seria DESC LIMIT 1", *m_db);

                if (query.next()) {
                    seria = query.value(0).toInt() + 1;
                } else {
                    seria = 100 + (QRandomGenerator::global()->generate() % 1000);
                    // Error, fail-safe value
                }
            } else {
                m_db->exec(settings.get("yearTable").toString().replace("[name]", "t" + year));

                auto yearItem = new QTreeWidgetItem();
                yearItem->setText(0, year);

                m_list->addTopLevelItem(yearItem);
            }

            date += " " + QString::number(seria);

            auto yearItem = m_list->findItems(year, Qt::MatchExactly)[0];

            QTreeWidgetItem* monthItem = nullptr;
            const QString monthStr = getMonth(date.mid(3,2).toInt());
            for (int i=0; i < yearItem->childCount(); ++i) {
                if (yearItem->child(i)->text(0) == monthStr) {
                    monthItem = yearItem->child(i);
                    break;
                }
            }

            if (!monthItem) {
                monthItem = new QTreeWidgetItem;
                monthItem->setText(0, monthStr);
                yearItem->addChild(monthItem);
            }

            auto dateItem = new QTreeWidgetItem;
            dateItem->setText(0, date);
            monthItem->addChild(dateItem);

            emit dateChanged(date);
        });
    });

    m_showAllCheckbox = new QCheckBox("Afiseaza doar ultimul anul");
    m_showAllCheckbox->setChecked(true);

    connect(m_showAllCheckbox, &QCheckBox::stateChanged, [=](int state){
        initialize(state != Qt::CheckState::Checked);
    });

    QPointer<QVBoxLayout> m_layout = new QVBoxLayout;
    m_layout->addWidget(m_list);
    m_layout->addWidget(m_showAllCheckbox);
    m_layout->addWidget(addMonthBtn);
    this->setLayout(m_layout);
}

void DateSelectComponent::setDatabase(const std::shared_ptr<QSqlDatabase> db) {
    m_db = db;
    initialize(false);
}

void DateSelectComponent::initialize(bool getAllYears) {
    if (m_list->topLevelItemCount()) {
        m_list->clear();
    }

    m_years = m_db->tables().filter(QRegExp("t[0-9]{4}")).replaceInStrings("t", "");

    if (m_years.empty()) return;

    auto years = m_years;

    if (!getAllYears) {
        years = QStringList() << years.last();
    }

    for (auto year : years) {
        auto item = new QTreeWidgetItem();
        item->setText(0, year);

        m_list->addTopLevelItem(item);

        QHash<int, QStringList> dates;
        QList<int> months;

        QSqlQuery query("SELECT DISTINCT Data, Seria FROM t" + year + " ORDER BY Data ASC", *m_db);
        while (query.next()) {
            auto date = query.value(0).toString();

            auto month = date.mid(3, 2).toInt();
            if (!months.contains(month)) {
                dates.insert(month, QStringList());
                months.append(month);
            }

            dates[month].append(date + " " + query.value(1).toString());
        }

        for (auto month : months) {
            auto monthItem = new NoSortTreeWidgetItem();
            monthItem->setText(0, getMonth(month));

            qDebug() << month << getMonth(month);

            item->addChild(monthItem);

            for (auto date : dates[month]) {
                auto dateItem = new QTreeWidgetItem();
                dateItem->setText(0, date);

                monthItem->addChild(dateItem);
            }
        }
    }

    m_list->sortItems(0, Qt::AscendingOrder);

    auto lastTopItem = m_list->topLevelItem(m_list->topLevelItemCount() - 1);
    lastTopItem->setExpanded(true);

    auto monthItem = lastTopItem->child(lastTopItem->childCount() - 1);
    if (monthItem) monthItem->setExpanded(true);
}

QString DateSelectComponent::getMonth(int monthNo) {
    switch (monthNo) {
        case 1: return "Ianuarie";
        case 2: return "Februarie";
        case 3: return "Martie";
        case 4: return "Aprilie";
        case 5: return "Mai";
        case 6: return "Iunie";
        case 7: return "Iulie";
        case 8: return "August";
        case 9: return "Septembrie";
        case 10: return "Octombrie";
        case 11: return "Noiembrie";
        case 12: return "Decembrie";
    }

    return "Error";
}

int DateSelectComponent::getMonth(const QString& month) {
    if (month == "Ianuarie") return 1;
    if (month == "Februarie") return 2;
    if (month == "Martie") return 3;
    if (month == "Aprilie") return 4;
    if (month == "Mai") return 5;
    if (month == "Iunie") return 6;
    if (month == "Iulie") return 7;
    if (month == "August") return 8;
    if (month == "Septembrie") return 9;
    if (month == "Octombrie") return 10;
    if (month == "Noiembrie") return 11;
    if (month == "Decembrie") return 12;

    return -1;
}
