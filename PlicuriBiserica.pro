QT += core gui sql printsupport widgets charts

TARGET = PlicuriBiserica
TEMPLATE = app
CONFIG += c++14

SOURCES += src/main.cpp \
    src/MainWindow.cpp \
    src/DateViewComponent.cpp \
    src/DateSelectComponent.cpp \
    src/MyTableWidget.cpp \
    src/YesNoDialog.cpp \
    src/MembersWindow.cpp \
    src/StatisticsComponent.cpp \
    src/SettingsWindow.cpp \
    src/Settings.cpp \
    src/WelcomeWindow.cpp \
    src/NoSortTreeWidgetItem.cpp \
    src/MembersHistoryWindow.cpp

HEADERS += \
    src/MainWindow.hpp \
    src/DateViewComponent.hpp \
    src/DateSelectComponent.hpp \
    src/MyTableWidget.hpp \
    src/MemberData.hpp \
    src/YesNoDialog.hpp \
    src/MembersWindow.hpp \
    src/StatisticsComponent.hpp \
    src/SettingsWindow.hpp \
    src/Settings.hpp \
    src/WelcomeWindow.hpp \
    src/NoSortTreeWidgetItem.hpp \
    src/MembersHistoryWindow.hpp
